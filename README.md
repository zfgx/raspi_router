#把树莓派打造成智能无线路由器

###使用方法

你可以 用git检出一份

    apt-get install git
    git clone http://git.oschina.net/ksc/raspi_router
或者直接下载zip压缩包

    wget http://git.oschina.net/ksc/raspi_router/repository/archive?ref=master -O t.zip
    unzip t.zip
    cd raspi_router/
    
check.py 检查你前几步的设置是否有效
若你用的是openvpn 一般不需要修改，直接运行即可
	
	#若网络设备名称和默认不一致，
	cp readme/config.example.ini config.ini 
	#将对应的设备名修改

    root@coolpi:~/raspi_route# ./check.py
    eth0 ok
    eth0_ip=192.168.2.11
    wlan0 ok
    wlan0_ip=192.168.10.1/24
    tun0 ok
    tun0_ip=10.2.2.10
    p-t-p:10.2.2.9
    检查通过 请运行一下命令生成 路由及转发脚本
    ./build_sh.py  

安照提示运行上述命令

    root@coolpi:~/raspi_route# ./build_sh.py
    /root/raspi_route/route.txt
    linux_route.sh 及 linux_nat.sh 已经生成 请运行
然后执行

    ./linux_route.sh && ./linux_nat.sh
    
以后若ip信息都没有变化的话 ，直接运行如下命令即可

    ifup wlan0 #若wlan0已经启用并获取ip地址了、则不用执行此命令
    service hostapd start
    service isc-dhcp-server start
    openvpn client.conf #具体看你的配置文件而定
    ./linux_route.sh 
    ./linux_nat.sh
###最后
>1. route表是我从网上搜集的,只有几个常用的网段，不是很全。
>2. 上述过程我检查了几遍，但难免有疏漏的地方。若您发现请指正
