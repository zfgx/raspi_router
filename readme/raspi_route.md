本文的目的是教大家访问特定网站时走加密线路(vpn)时更加方便不用来回切换网络
#前提

能有正常上网的环境 即pi插上网线后就能上网（如何上网不在此文讨论范围）

  
>1. 树莓派一个 （B型的带有线网卡）
>2. USB无线网卡一个（ 我的是在京东上买的 二十块钱左右）
>3. vpn账户一个，并且能正常连通上网

我这里用的是openvpn，如果你有国外vps 如何配置请查看  [OpenVPN安装配置教程](http://www.vpser.net/build/linode-install-openvpn.html) 

在这里我只简单说下客户端的配置

#开始配置
    ifconfig 
    eth0      Link encap:Ethernet  HWaddr b8:27:eb:3c:2b:11
              inet addr:192.168.2.11  Bcast:255.255.255.255  Mask:255.255.255.0
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:47072 errors:0 dropped:0 overruns:0 frame:0
              TX packets:48828 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000
              RX bytes:29137358 (27.7 MiB)  TX bytes:9871271 (9.4 MiB)

    lo        Link encap:Local Loopback
              inet addr:127.0.0.1  Mask:255.0.0.0
              UP LOOPBACK RUNNING  MTU:16436  Metric:1
              RX packets:693 errors:0 dropped:0 overruns:0 frame:0
              TX packets:693 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:0
              RX bytes:121328 (118.4 KiB)  TX bytes:121328 (118.4 KiB)


    wlan0     Link encap:Ethernet  HWaddr c8:3a:35:c1:29:ab
              inet addr:192.168.10.1  Bcast:192.168.10.255  Mask:255.255.255.0
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:29999 errors:0 dropped:0 overruns:0 frame:0
              TX packets:27439 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000
              RX bytes:3228721 (3.0 MiB)  TX bytes:27597707 (26.3 MiB)


eth0是我的有线网卡 已经获自动获取了ip（我现在就是通过有线ssh控制pi的）

wlan0就是那个无线网卡了（一般情况下是wlan0，若没有的话，你可以输入lsusb，看一下有没有显示出 你的无线网卡设备 。没有的就是驱动问题。）

修改 网络配置文件 给wlan0设置固定ip 在这里我设置的是192.168.10.1

**注意一定不要和eth0在同一个网段**
**另外若你启用了wicd等网络管理服务请关闭**

    root@coolpi:~# cat /etc/network/interfaces
    auto lo

    iface lo inet loopback
    iface eth0 inet dhcp

    #allow-hotplug wlan0
    #iface wlan0 inet manual
    #wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
    #iface default inet dhcp

    iface wlan0 inet static
    address 192.168.10.1
    netmask 255.255.255.0
##1 vpn客户端配置
安装openvpn，这里我们只是用它的客户端功能（如何配置，看上面的连接）  
    
    apt-get install openvpn
下面是我的配置文件

    client
    dev tun       #要与前面server.conf中的配置一致。
    proto udp              #要与前面server.conf中的配置一致。
    remote chicago.vps 8080    #将服务器IP替换为你的服务器IP，端口与前面的server.conf中配置一致。

    resolv-retry infinite
    nobind
    persist-key
    persist-tun

    ca ca.crt
    cert client.crt
    key client.key
    ns-cert-type server

    #是否作为网关出口，会影响路由表的默认出口
    ;redirect-gateway

    keepalive 20 60
    #tls-auth ta.key 1
    comp-lzo
    verb 3
    mute 20
    route-method exe
    route-delay 2
需要注意的是 

    ca ca.crt
    cert client.crt
    key client.key
这几个证书文件最好写成得路径，否则执行命令

    openvpn /etc/openvpn/client.conf 
    
会提示找不到这几个文件
只有cd /etc/openvpn/ 然后 openvpn client.conf 才行
若配置的没有有问题的话 输入ifconfig 会多出来一个虚拟网络设备tun0

    tun0      Link encap:UNSPEC  HWaddr 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00
              inet addr:10.2.2.10  P-t-P:10.2.2.9  Mask:255.255.255.255
              UP POINTOPOINT RUNNING NOARP MULTICAST  MTU:1500  Metric:1
              RX packets:22212 errors:0 dropped:0 overruns:0 frame:0
              TX packets:18882 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:100
              RX bytes:26477654 (25.2 MiB)  TX bytes:1607636 (1.5 MiB)
          
记住这两个地址 inet addr:10.2.2.10  P-t-P:10.2.2.9 待会要用到

##2 安装路由软件
下一步就是安装两个软件hostapd isc-dhcp-server
 
####1) AP热点 hostapd 服务

    #安装
    apt-get install hostapd
    #编辑默认配置文件路径
    vi /etc/default/hostapd
    将
    #DAEMON_CONF= ""
    这一行修改为
    DAEMON_CONF="/etc/hostapd/hostapd.conf"

    #解压默认配置文件
    zcat /usr/share/doc/hostapd/examples/hostapd.conf.gz >/etc/hostapd/hostapd.conf
    #修改默认配置文件 
    vi /etc/hostapd/hostapd.conf 
刚开始的时候可以不加密直接使用默认配置就行 只需要修改下 ssid 和channel

**注意: 默认是不加密的连接，建议等你完全测试成功了再修改为wpa加密的(配置文件中有各种模版)**

    ssid= test_ap #搜索无线信号时看到的那个
    channel = 11 #此值最好不要与其他无线路由器重复。但是我试了一下别的信道 13、12等 hostapd 提示错误 貌似是网卡不支持 

####2) ip地址自动分配 dhcpd服务

    #安装
    apt-get install isc-dhcp-server
    #将一下配置添加进  /etc/dhcp/dhcpd.conf 
    
    subnet 192.168.10.0 netmask 255.255.255.0 {
      range 192.168.10.10 192.168.10.100; #分配给客户端的ip范围
      option routers 192.168.10.1;#网关地址 
      option broadcast-address 192.168.10.255; #广播地址 注意和上面的子网掩码255.255.255.0 对应
      option domain-name-servers 8.8.8.8,8.8.4.4; #DNS服务器地址
      default-lease-time 600; #租约时间 到期后客户端需要重新发起请求获取ip地址（可以续约原来的ip）
      max-lease-time 7200;
    }
**注意:**

>1. 配置文件中的网段，子网掩码，以及网关要和给wlan0设置的静态ip【相对应】wlan0就是这个网段的网关）
>2. dns目前最好是配置成google的dns。因为国内的在根上都被污染了， google的dns走vpn的话没问题不会被污染，相应的解析速度也会变慢 

（还有一种解决办法就是修改hosts，但是这个太麻烦，国外大网站不止一个域名，ip地址也是多个，收集这些信息也挺麻烦，相对来说收集国外网站ip段来说还是容易的）


####3) 全部配置完毕后 ，启动服务
 
    ifup wlan0 #启用网卡
    service hostapd  start
or    
    hostapd -ddK /etc/hostapd.conf #方便查看调试信息
    
    service isc-dhcp-server  start #注意在这一步的时候一定要确保wlan0已经启用并配置好了ip地址 不然会dhcpd会报错

 
####4. 连接测试
现在你拿出手机搜索一下无线信号 有没有 [test_ap] 然后连接上试试。

若能成功连接并且获取ip，则表明你的pi无线路由器了

但是现在你得到手机还不能上网
需要设置一下**NAT转发**，即把wlan0上的网络连接转发到eth0上面通过有线来上网

##3. 路由转发
----------分割线下面这些 觉得麻烦的可以不看，我在后面提供了一个脚本 不过最好还是看一下，有助于理解-------------

* 1) 
    将192.168.10.0/24 这个网段的流量转发到 eth0上
    注意：192.168.2.11 是eth0的ip

        iptables -t nat -A POSTROUTING -s 192.168.10.0/24 -j SNAT --to-source 192.168.2.11

    目前位置，手机连上无线后应该就能上网了，但访问国外网站还是不行的

* 2) 
         
         iptables -t nat -F #先把上面那条规则删除了
         
    下面事例中我用8.8.8.8 代指国外ip ，你可以任意修改成*“任意国外ip地址”*

    将192.168.10.0/24 并且是去往8.8.8.8(任意国外ip地址) 的流量通过 10.2.2.10转发出去

        iptables -t nat -A POSTROUTING -s 192.168.10.0/24 --dst 1.1.1.1 -j SNAT  --to-source 10.2.2.10
    
* 3)
    注意：目前默认的网关仍然是eth0 192.168.2.1
    看一下下面的路由表 默认是从 192.168.2.1出去的

        root@coolpi:~/raspi_router# route
        Kernel IP routing table
        Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
        default         192.168.2.1     0.0.0.0         UG    0      0        0 eth0
        10.2.2.0        10.2.2.9        255.255.255.0   UG    0      0        0 tun0
        10.2.2.9        *               255.255.255.255 UH    0      0        0 tun0
        192.168.2.0     *               255.255.255.0   U     0      0        0 eth0
        192.168.10.0    *               255.255.255.0   U     0      0        0 wlan0

    所以我们要添加一个路由项 （10.2.2.9 就是上面tun0 p-t-p 那一项，我的理解就是vpn的虚拟网关）。
    把去往8.8.8.8的流量路由到vpn上面去，不走原来的路线了

        route add -net 8.8.8.8/32 gw 10.2.2.9 dev tun0
        root@coolpi:~/raspi_route# traceroute 8.8.8.8
        traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
         1  10.2.2.1 (10.2.2.1)  306.001 ms  308.014 ms  308.195 ms
         2  省略……
    用traceroute命令可以看到 现在走的是vpn

    若你只设置8.8.8.8的路由而不设置该ip的NAT转发，在pi上没有什么问题，会按照你的设置自动选择线路

    但是无线网络下面的终端（手机等）还是只能访问国内的ip地址 你设置的“国外ip”将不能访问（假如原来能访问的话hehe）

    总的来说 路由规则,NAT转发缺一不可

        route add -net 8.8.8.8/32 gw 10.2.2.9 dev tun0
        iptables -t nat -A POSTROUTING -s 192.168.10.0/24 --dst 8.8.8.8 -j SNAT  --to-source 10.2.2.10
    
    
    ![简略拓扑图](http://blog.geekli.cn/wp-content/uploads/2013/06/vpn.bmp)
    
* 4)
    现在国外的网站能访问了，我们再把第一步在重复一遍 设置默认转发规则
    **注意：这一步一定要放到最后执行 **iptables规则就像一个筛子 第一层被挡住以后就不会往下走了
    
        iptables -t nat -A POSTROUTING -s 192.168.10.0/24 -j SNAT --to-source 192.168.2.11

    
-----------------------------结束--------------------------------


我写了个脚本,python写的（shell不是很熟悉）

    check.py
    build_sh.py #根据route.txt 生成 添加路由表和 iptables NAT转发命令
    route.txt #国外的ip网段，不是很全
可以把一些网段自动转换称shell脚本 已经传到了 [git@osc](http://git.oschina.net/ksc/raspi_router) 上面

###使用方法

你可以 用git检出一份

    apt-get install git
    git clone http://git.oschina.net/ksc/raspi_router
或者直接下载zip压缩包

    wget http://git.oschina.net/ksc/raspi_router/repository/archive?ref=master -O t.zip
    unzip t.zip
    cd raspi_router/
    
check.py 检查你前几步的设置是否有效
若你用的是openvpn 一般不需要修改，直接运行即可

    root@coolpi:~/raspi_route# ./check.py
    eth0 ok
    eth0_ip=192.168.2.11
    wlan0 ok
    wlan0_ip=192.168.10.1/24
    tun0 ok
    tun0_ip=10.2.2.10
    p-t-p:10.2.2.9
    检查通过 请运行一下命令生成 路由及转发脚本
    ./build_sh.py  

安照提示运行上述命令

    root@coolpi:~/raspi_route# ./build_sh.py 
    /root/raspi_route/route.txt
    linux_route.sh 及 linux_nat.sh 已经生成 请运行
然后执行

    ./linux_route.sh && ./linux_nat.sh
    
以后若ip信息都没有变化的话 ，直接运行如下命令即可

    ifup wlan0 #若wlan0已经启用并获取ip地址了、则不用执行此命令
    service hostapd start
    service isc-dhcp-server start
    openvpn client.conf #具体看你的配置文件而定
    ./linux_route.sh 
    ./linux_nat.sh
###最后
>1. route表是我从网上搜集的,只有几个常用的网段，不是很全。
>2. 上述过程我检查了几遍，但难免有疏漏的地方。若您发现请指正
